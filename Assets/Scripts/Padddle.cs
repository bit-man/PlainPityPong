﻿using UnityEngine;

public class Padddle : MonoBehaviour
{
    private static float speed = 0.3f;

    public bool leftPaddle;
    private Transform t;

    void Start()
    {
        t = GetComponent<Transform>();
    }

    void FixedUpdate()
    {
        float move;
        if (leftPaddle)
        {
            move = Input.GetAxis("Vertical_P1") * speed;
        }
        else
        {
            move = Input.GetAxis("Vertical_P2") * speed;
        }

        t.position = new Vector3(t.position.x, t.position.y + move, t.position.z);
    }

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    private float speed = 0.1f;
    private Transform location;

    void Start()
    {
        location = GetComponent<Transform>();
    }

    void FixedUpdate()
    {
        var position = location.position;
        location.transform.position = new Vector3(position.x + speed, position.y, position.z);
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
        speed = -speed;
        location.rotation = new Quaternion(0,0,0,0); // avoids ball rotation on edge colision
        Debug.Log("Collision (ball) !!!");
    }
}